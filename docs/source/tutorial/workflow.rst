=========================
Workflow for OpFlowLab
=========================

.. .. thumbnail:: images/workflow.png
   :alt: Typical workflow for OpFlowLab

A typical workflow for OpFlowLab is as follows.

(0) Loading of images / velocity field
(1) Optical flow calculation
(2) FlowMatch
(3) FlowWarp and/or FlowTracer
(4) FlowPath
(5) FlowDerivative

.. tip::

  A YAML file (can be opened in any text editor) is created in the directory of the loaded image file.
  It lists the actions taken during each session.
  This will help you to trace your steps at a later time point if needed.

  In addition, the text in the console can be saved at any time by pressing the text labelled "Save console log".


------------

.. _loading:

Loading images
========================

.. thumbnail:: images/image_loading.png
  :alt: GUI for image loading

GUI/options guide: :any:`loading_gui`

**Loading a single tif image containing multiple time slices:**

- Load the image into OpFlowLab either by inputting the path into the input box or select the directory icon and locate the file in your directory.
- Press the "Load image" button (or "Reload image" if an image has been previously loaded).
- There should be a confirmation that the image is loaded in the console.
- You can verify that the image is loaded properly by pressing the "Display loaded image" button.
- Press the 'Next' button when you are ready to move to the next step.


------------

.. _opflow:

(1) Optical flow calculations
================================

.. thumbnail:: images/opflow_calculation_1.png
  :alt:

GUI/options guide: :any:`opflow_gui`

**Performing optical flow calculations in the forward time direction:**

- Ensure that the path to the OpFlowLab executable is correct.
- Select the optical flow algorithm to use from the dropdown menu. See the tip box *Choosing the right optical flow algorithm?* below.
- Perform the optical flow calculations by pressing the button 'Calculate optical flow velocity field'.
- When the optical flow calculations are done press the 'Next' button.

**Performing optical flow calculations in the reverse time direction:**

- Ensure that the "Reverse time direction" checkbox is selected.
- Perform the same steps as when calculating velocities in the forward time direction.

.. tip:: **Choosing the right optical flow algorithm?**

  The choice of optical flow algorithm is important to ensure that the velocity field is as accurate as possible.
  We recommend trying **NvidiaOF** on your dataset first if your hardware satisfies the requirements (see :any:`requirements`).
  If not, using the default settings for **Farneback** would also serve as a good starting point.


------------

.. _load_velocity:

Load velocity field
================================

.. thumbnail:: images/load_velocities.png

GUI/options guide: :any:`velocity_gui`

**Select the forward velocity field folder:**

- Input the path in the text box labelled "Forward velocity field folder" or select the folder containing the velocity field calculated in (1).
  This folder should contain the folders "flowx" and "flowy".

**Select the reverse velocity field folder:**

- Check the checkbox labelled "Reverse velocity field folder".
- Input the path in the text box beside it or select the folder containing the velocity field calculated in (1).
  This folder should contain the folders "flowx" and "flowy".

**Select the object segmentation folder:**

- Check the checkbox labelled "Object segmentation folder".
- Input the path in the text box beside it or select the folder containing the labeled image.
  This folder should contain labeled images with each object assigned a unique value.

**Load the folders:**

- Press the "import velocities" button.
- There should be a confirmation that the folders are loaded in the console.

.. tip:: **What folders should be loaded?**

  Each routine has a different requirement for which folders should be loaded. Below are the list of required folders for each routine:

  - **FlowMatch** -- Image folder, forward velocity folder, reverse velocity folder and object segmentation folder.
  - **FlowWarp** -- Image folder and forward velocity folder.
  - **FlowTracer** -- Image folder and forward velocity folder. Object segmentation folder is needed if the "object centroids" option is selected.
  - **FlowPath** -- Image folder and forward velocity folder.
  - **FlowDerivative** -- Image folder and forward velocity folder.


------------

.. _FlowMatch:

(2) FlowMatch -- Object matching
================================

.. thumbnail:: images/FlowMatch.png
  :alt:

GUI/options guide: :any:`FlowMatch_gui`

FlowMatch uses the loaded velocity fields (forward and reverse time direction) to match labelled images across two time frames.
This matching is then used to refine the velocity field.

**Perform object matching using the FlowMatch routine:**

- Ensure that the forward velocity, reverse velocity, and object segmentation folders are loaded.
- The default values are a good starting point for object matching.
- Run FlowMatch by pressing the "Perform FlowMatch" button.
- The matched velocity is saved in a new "FlowMatch" folder in the directory with the loaded image file.
- When the FlowMatch is done press the 'Next' button to move to velocity field validation using FlowWarp.


------------

.. _FlowWarp:

(3a) FlowWarp -- Velocity field validation
==========================================

.. thumbnail:: images/FlowWarp.png
  :alt:

GUI/options guide: :any:`FlowWarp_gui`

FlowWarp allows for the visual comparison between an image that is warped with the forward velocity field and the next image in the time sequence.
By overlaying the two images as separate colours, areas that are in agreement shows up as a composite of the two colours,indicating that the velocity fields at these points are reasonable.
This facilitates the validation of the vector fields.

**Perform vector field validation using the FlowWarp routine:**

- Ensure that the forward velocity folder is loaded.
- Run FlowWarp by pressing the "Generate FlowWarp" button.
- The output for FlowWarp can be visualised by pressing the "Display FlowWarp" button.
- The image file can be found in a new "FlowWarp" folder in the forward velocity directory.


------------

.. _FlowTracer:

(3b) FlowTracer -- Velocity field validation
============================================

.. thumbnail:: images/FlowTracer.png
  :alt:

GUI/options guide: :any:`FlowTracer_gui`

FlowTracer seeds artificial tracers into the image before updating their positions using the loaded velocity field.
If the velocity field is reasonable, we would expect that the artificial tracers would follow the movement of objects in the image.

**Perform vector field validation using the FlowTracer routine:**

- Ensure that the forward velocity folder is loaded.
- Select the type of tracer to generate by clicking the dropdown menu beside "Tracer generation type". The types of initial tracers are as follows:

  - Random tracers -- Sets the initial location of tracers to a random location.
  - Grid tracers -- Set the initial location of the tracers based on a grid of a specified distance.
  - Object centroids -- Sets the initial location of tracers to the centroids of the objects in the image. If this is selected, please ensure the object segmentation folder is loaded.

- Run FlowTracer by pressing the "Generate FlowTracer" button.
- The output for FlowTracer can be visualised by pressing the "Display FlowTracer" button.
- The image file can be found in a new "FlowTracer" folder in the forward velocity directory.


------------

.. _FlowPath:

(4) FlowPath -- Velocity field visualization
============================================

.. thumbnail:: images/FlowPath.png
  :alt:

GUI/options guide: :any:`FlowPath_gui`

FlowPath draws the pathlines in the velocity field by plotting the path taken by artificial tracers over multiple time frames.
This provides a visual of the movement of objects over time.

**Perform vector field visualization using the FlowPath routine:**

- Ensure that the forward velocity folder is loaded.
- By default, the path will be colorized based on the average angle of the path using the viridis colormap.
- Run FlowPath by pressing the "Generate FlowPath" button.
- The output for FlowPath can be visualised by pressing the "Display FlowPath" button.
- The image file and colour map can be found in a new "FlowPath" folder in the forward velocity directory.


------------

.. _FlowDerivative:

(5) FlowDerivative -- Velocity field derivatives
================================================

.. thumbnail:: images/FlowDerivative.png
  :alt:

GUI/options guide: :any:`FlowDerivative_gui`

FlowDerivatives calculates various derivatives that are useful for determining if there are any underlying structures, such as vortices, in the velocity field.

**Calculate vector field derivatives using the FlowDerivative routine:**

- Ensure that the forward velocity folder is loaded.
- Select the type of derivative to be calculated using the "Derivative type" drop down menu.
- Run FlowDerivative by pressing the "Process FlowDerivative" button.
- The output for FlowDerivative can be visualised by pressing the "Display FlowDerivative" button.
- The image file and colour map can be found in a new folder labelled with the derivative type in the forward velocity directory.
