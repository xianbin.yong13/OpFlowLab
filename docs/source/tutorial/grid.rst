=========================
Parameter grid search
=========================

.. thumbnail:: images/grid_label.png
  :alt: Example of a grid parameter search

OpFlowLab provides a simple grid search algorithm.

To define the grid search, the following should be inputted into the text parameter fields in the format as indicated in (1)::

  $ start-stop-step

This follows the same convention as Python where the start value is included and the stop value is omitted.