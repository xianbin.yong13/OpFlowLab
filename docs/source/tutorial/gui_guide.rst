====================================
OpFlowLab's graphical user interface
====================================

.. thumbnail:: images/main_window.png
  :alt: Main components of the OpFlowLab's graphical user interface

(1) Menu bar — Allows for the selection of the various routines in the workflow.
(2) Parameters — Allows for the selection of parameters for each routine in the workflow.
(3) Console output — Shows the output generated from each routine. The console log can be saved by clicking the button labelled "Save console log".

------------

.. _loading_gui:

Loading images
========================

.. thumbnail:: images/image_loading_label.png
  :alt: Description of the various components of the image loading process

(1)
  **(a)** Input box to key in path to image stack.
  **(b)** Opens a dialog box to select the image stack that is to be analysed.
(2) Loads the image stack.
(3) Displays the loaded image stack. Enabled only when an image stack has been loaded.
(4) Moves to the next tab (Optical flow calculation). Enabled only when an image stack has been loaded.
(5) Console output indicating the file that has been loaded.

.. attention::
  Currently supported image types: \*.tif.

  Please ensure that this tiff image is a single image stack with all the time frames to be analysed.

Go to workflow: :any:`loading`

------------

.. _opflow_gui:

Optical flow calculations
================================

.. thumbnail:: images/optical_flow.png
  :alt: Description of the various key components for optical flow calculations

(1) Location of the optical flow executable. Please ensure that this path is valid under the "Advanced config" tab.
(2) Selects the optical flow algorithm to use.
(3) Save each vector using a float16 format instead of a float32 format. The effect on precision should be minimal but provides a significant improvement in vector field saving and loading whilst saving disk space.
(4) When checked, performs optical flow calculation in the reverse image stack order (reverse time direction).
(5) When checked, saves the velocity files as a tif image instead of a binary file.
(6) Calculates the velocity field using the specified optical flow algorithm.
(7) Moves to the next tab (Load velocity field). Enabled only when the optical flow calculations are completed.

.. tip:: **Determining the optimal set of parameters?**

  Determining the optimal set of parameters can be challenging as it is generally problem dependent.
  The default settings can serve as a good starting point to try out the various algorithms.
  However, we strongly recommend that a parameter search be done to determine the optimal parameters.
  We have provided an implementation of a grid search in OpFlowLab but other search algorithms can be used instead to speed up the time taken to determine the optimal parameters.

Farneback optical flow
----------------------

.. thumbnail:: images/farneback_label.png
  :alt: Options for the Farneback optical flow

(1) Specifies the number of pyramid levels to use.
(2) Specifies the averaging window size that is used to reduce noise in the velocity field at the expense of a smoother field.
(3) Specifies the number of iterations to perform per pyramid level to refine the velocity field.
(4) Specifies the amount to scale the image for each pyramid level.
(5) Specifies the size of the pixel neighbourhood used in polynomial expansion.
(6) Specifies the standard deviation of the Gaussian applicability function.

Dense Pyramidal Lucas Kanade optical flow
-----------------------------------------

.. thumbnail:: images/pyrlk_label.png
  :alt: Options for the Dense Pyramidal Lucas Kanade optical flow

(1) Specifies the number of pyramid levels to use.
(2) Specifies the pixel neighbourhood to calculate the least square estimated of velocity for each pixel.
(3) Specifies the number of iterations to perform per pyramid level to refine the velocity field.

Nvidia hardware optical flow
----------------------------

.. thumbnail:: images/nvof_label.png
  :alt: Options for the Nvidia hardware optical flow

(1) Specifies the preset used in calculating the velocity field.

Go to workflow: :any:`opflow`

------------

.. _velocity_gui:

Load velocity fields
========================

.. thumbnail:: images/load_velocities2_label.png
  :alt: Description of the various components of the velocity field loading process

(1) Specifies the folder containing the forward time direction velocity field.
(2) When checked, specifies the folder containing the reverse time direction velocity field.
(3) When checked, specifies the folder containing the labelled images.
(4) Specifies if the velocity field is dense (1 velocity per pixel) or sparse (1 velocity per window).
(5) Specifies if the velocity field is in the float16 or float32 format.
(6) Specifies the extension of the velocity field.
(7) Specifies if the neighbourhood size of the median filter. Set to None to not perform median filtering.
(8) Loads the respective folders.
(9) Moves to the next tab (FlowMatch).

Go to workflow: :any:`load_velocity`

------------

.. _FlowMatch_gui:

(2) FlowMatch
========================

.. thumbnail:: images/FlowMatch_label.png
  :alt: Description of the various components of FlowMatch routine

(1) Location of the forward velocity fields. Please ensure that this path is valid under the "Load velocity field" tab.
(2) Location of the reverse velocity fields. Please ensure that this path is valid under the "Load velocity field" tab.
(3) Location of the labelled images. Please ensure that this path is valid under the "Load velocity field" tab.
(4) Specifies the threshold used to determine valid pairs. This should be in the same order of magnitude (in pixels) of the movement.
(5) Specifies the minimum size of the object that should be included in the FlowMatch process. Set to "None" to ignore.
(6) Specifies the maximum size of the object that should be included in the FlowMatch process. Set to "None" to ignore.
(7) Specifies if the vectors should be saved as a 32 bit tiff image instead of a binary file. (Not recommended)
(8) Performs FlowMatch with the parameters specified above.
(9) Moves to the next tab (FlowWarp).

Go to workflow: :any:`FlowMatch`

------------

.. _FlowWarp_gui:

(3a) FlowWarp
========================

.. thumbnail:: images/FlowWarp_label.png
  :alt: Description of the various components of FlowWarp routine

(1) Location of the forward velocity fields. Please ensure that this path is valid under the "Load velocity field" tab.
(2) Specifies the starting frame to perform the FlowWarp routine from. Used when multiple time frames are loaded.
(3) Generates FlowWarp images with the parameters specified above.
(4) Displays the FlowWarp image in a separate window.
(5) Moves to the next tab (FlowTracer).

Go to workflow: :any:`FlowWarp`

------------

.. _FlowTracer_gui:

(3b) FlowTracer
========================

.. thumbnail:: images/FlowTracer_label.png
  :alt: Description of the various components of FlowTracer routine

(1) Location of the forward velocity fields. Please ensure that this path is valid under the "Load velocity field" tab.
(2) Location of the labelled images. Please ensure that this path is valid under the "Load velocity field" tab if "Object centroid" is used to generate the tracer locations.
(3) Specifies the method used to generate the initial tracers.
(4) Specifies if the initial tracer positions should be saved alongside the FlowTracer images.
(5) Specifies the number of tracers to generate. This is only enabled when "Random tracers" is selected.
(6) Specifies the distance between grid points used to generate the tracers. This is only enabled when "Grid tracers" is selected.
(7) Performs FlowTracer with the parameters specified above.
(8) Displays the FlowTracer image in a separate window.
(9) Moves to the next tab (FlowPath).

Go to workflow: :any:`FlowTracer`

------------

.. _FlowPath_gui:

(4) FlowPath
========================

.. thumbnail:: images/FlowPath_label.png
  :alt: Description of the various components of FlowPath routine

(1) Location of the forward velocity fields. Please ensure that this path is valid under the "Load velocity field" tab.
(2) Specifies the maximum number of frames that will be used to draw each path in the FlowPath image.
(3) Specifies the starting frame to perform the FlowPath routine from. Used when multiple time frames are loaded.
(4) Specifies the distance between grid points used to initialize the starting positions of the FlowPaths.
(5) Specifies the alpha value of the FlowPath image. This produces an image showing the FlowPath as an overlay over the original image. Set to "None" to just output the FlowPath image.
(6) Specifies the metric used to colourise the FlowPath image.
(7) Specifies the colour map used to colourise the FlowPath image.
(8) Specifies the lower bound of the colour map. Set to "None" to use the minimum value of the image.
(9) Specifies the upper bound of the colour map. Set to "None" to use the maximum value of the image.
(10) Performs FlowPath with the parameters specified above.
(11) Displays the FlowPath image in a separate window.
(12) Displays the colour map used to generate the image.
(13) Moves to the next tab (FlowDerivative).

Go to workflow: :any:`FlowPath`

------------

.. _FlowDerivative_gui:

(5) FlowDerivative
========================

.. thumbnail:: images/FlowDerivative_label.png
  :alt: Description of the various components of FlowDerivative routine

(1) Location of the forward velocity fields. Please ensure that this path is valid under the "Load velocity field" tab.
(2) Specifies the derivative that should be calculated from the velocity field.
(3) Specifies the starting frame to perform the FlowDerivative routine from.
(4) Specifies the sigma of the gaussian smoothing function. This should be set to a value 2x the average distance between objects.
(5) Specifies the colour map used to colourise the FlowPath image.
(6) Specifies the lower bound of the colour map. Set to "None" to use the minimum value of the image.
(7) Specifies the upper bound of the colour map. Set to "None" to use the maximum value of the image.
(8) Performs FlowDerivative with the parameters specified above.
(9) Displays the FlowDerivative image in a separate window.
(10) Displays the colour map used to generate the image.

Go to workflow: :any:`FlowDerivative`

------------

.. _advanced_gui:

Advanced config
================================

.. thumbnail:: images/advanced_label.png
  :alt: Description of the various key components under the advanced config tab

(1) Input to specify the Location of the optical flow executable.
(2) Specify the name of the folder to create for the FlowMatch routine.
(3) Specify the name of the folder to create for the FlowWarp routine.
(4) Specify the name of the folder to create for the FlowPath routine.
(5) Specify the name of the folder to create for the FlowTracer routine.
(6) Saves the configuration to a YAML file in the user's home directory.
