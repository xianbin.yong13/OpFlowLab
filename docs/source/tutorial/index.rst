=========================
OpFlowlab Workflow Guides
=========================

These guides will help you to be able to navigate through the graphical user interface and the options available.

.. toctree::
  :maxdepth: 2

  gui_guide
  workflow
  grid