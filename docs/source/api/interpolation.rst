Interpolation module
----------------------------------------

.. automodule:: OpFlowLab.functions.interpolation
   :members:
   :undoc-members:
