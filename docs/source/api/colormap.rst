Colormap module
----------------------------------------------

.. automodule:: OpFlowLab.functions.colormap_functions
   :members:
   :undoc-members:
