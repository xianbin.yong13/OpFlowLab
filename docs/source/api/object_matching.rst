Object matching module
-------------------------------------------

.. automodule:: OpFlowLab.functions.object_matching
   :members:
   :undoc-members:
