How to install OpFlowLab?
=========================



Installing
----------
OpFLowLab can be installed with pip::

    $ python -m pip install OpFLowLab

Alternatively, the latest source code is available from GitLab::

    $ git clone git@gitlab.com:xianbin.yong13/OpFlowLab.git
    $ python setup.py install

Setting up OpFlowLab for optical flow calculations
--------------------------------------------------
As not all of the OpenCV CUDA functionality is exposed via python bindings the following steps are still required.

**Step 1:** Please install the following.

- CUDA toolkit
    - https://developer.nvidia.com/cuda-11.1.1-download-archive
    - The express installation option would suffice.

**Step 2:** The following files need to be downloaded.

- OpenCV binaries

  - Windows: https://jamesbowley.co.uk/downloads/

    - We suggest that you use the release version of OpenCV 4.5.0 with CUDA 11.1.
    - 7-Zip or WinRAR is required to open the downloaded archive.

- CUDNN

  - https://developer.nvidia.com/cudnn
  - Membership to the NVIDIA Developer program is needed (Free to join).

- OpFlowLab executable

  - https://gitlab.com/xianbin.yong13/optical-flow-opencv/-/releases

    - Current version (0.0.3) is built for OpenCV 4.5.0 with CUDA 11.1.

.. attention:: **What is the "OpFlowLab executable"?**

  The OpFlowLab executable is an executable file that calls the required OpenCV functions to calculate the velocity field.
  This executable is called from OpFlowLab during the calculation of optical flow velocities.

  Certain web browsers (e.g. Microsoft Edge/Google Chrome) flags the file as suspicious due to the file being an executable with low download counts.
  However, the file is safe and can be whitelisted.

  Alternatively, the executable can be built from source.
  For more information, see building from source and https://gitlab.com/xianbin.yong13/optical-flow-opencv.

**Step 3:** Extract OpFlowLab executable to a new folder.

- Create a new folder in a convenient location (e.g. on the desktop).
- Place OpFlowLab executable in that folder.

**Step 4:** Place opencv binary in the same folder with the OpFlowLab executable.

- Extract OpenCV binaries (7-Zip can be installed from https://www.7-zip.org/).
- Locate opencv_world450.dll (in install/x64/vc16/bin/ for the extracted binaries).
- Place opencv_world450.dll in the same folder as the OpFlowLab executable.

**Step 5:** Place cuDNN dll in the same folder with the OpFlowLab executable.

- Extract cuDNN download.
- Locate cudnn64_8.dll (in cuda/bin/ for the extracted cuDNN download).
- Place cudnn64_8.dll in the same folder as the OpFlowLab executable.

**Step 6:** Point OpFlowLab to the directory containing the OpFlowLab executable.

- Start OpFlowLab and key in the location under the Advanced config tab.

  - GUI/options guide: :any:`advanced_gui`

Starting OpFlowLab
-------------------
The graphical user interface can be started with the following command::

    $ opflowlab

Troubleshooting
---------------