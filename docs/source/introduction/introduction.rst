What is OpFlowLab?
==================

The motivation behind OpFlowLab
-------------------------------

.. thumbnail:: example.gif

Optical flow algorithms have been shown to outperform particle image velocimetry for motion estimation especially in biological images.
OpFlowLab seeks to facilitate the use of optical flow algorithms on your dataset by making the calculation, validation and verification process as simple as a few clicks.

OpFlowLab provides the following:

- Simple graphical interface to assess CUDA optimized optical flow algorithms provided by OpenCV
- Post processing of velocities using FlowMatch, an object matching routine.
- Velocity field validation using artificial tracers and image warping
- Visualization of velocity pattern using pathlines
- Calculation of velocity field derivatives

